# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://AngelovK@bitbucket.org/AngelovK/stroboskop.git

```

Naloga 6.2.3:
https://bitbucket.org/AngelovK/stroboskop/commits/1eb2a282863124019e38968062bfce35e75d81fb

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/AngelovK/stroboskop/commits/58cff57576b7cc1ecfc9d5751ee21ec75c2f1a2c

Naloga 6.3.2:
https://bitbucket.org/AngelovK/stroboskop/commits/f1e365986d7507e2f475d2391321cedbe47330af

Naloga 6.3.3:
https://bitbucket.org/AngelovK/stroboskop/commits/b5b56a9546ec4352813d357449dbdfa12276820d

Naloga 6.3.4:
https://bitbucket.org/AngelovK/stroboskop/commits/0c0480347beda4c65734b6566a1377dc6b3f9e4f

Naloga 6.3.5:

```
git checkout master 
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/AngelovK/stroboskop/commits/f572856b6b876d149ad0a73904e41a9d704c75e8

Naloga 6.4.2:
https://bitbucket.org/AngelovK/stroboskop/commits/fda28635b6fdc6617caf08490938e56d6255e5d3

Naloga 6.4.3:
https://bitbucket.org/AngelovK/stroboskop/commits/ed9dbb8d4dd015bb628b49816ac40119f144b233

Naloga 6.4.4:
https://bitbucket.org/AngelovK/stroboskop/commits/eae312815a38a8f5dcd0b2609120e9ff6f2526d6